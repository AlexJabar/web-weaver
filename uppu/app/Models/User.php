<?php

namespace App\Models;
    
use Illuminate\Database\Eloquent\Model;//класс для работы с БД

class User extends Model
{
    protected $table = 'users';//Выбор таблицы БД

    protected $fillable = ['name', 'email', 'password'];//разрешения заполнения полей БД


    public function setPassword($password)
    {
        $this->update([
            'password' => password_hash($password, PASSWORD_DEFAULT),
        ]);
    }



}
