<?php

namespace App\Models;
    
use Illuminate\Database\Eloquent\Model;//класс для работы с БД

class File extends Model
{
    protected $table = 'files';//Выбор таблицы БД

    protected $fillable = ['name', 'type', 'size', 'url', 'hash', 'description'];//разрешения заполнения полей БД

}
