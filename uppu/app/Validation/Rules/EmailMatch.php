<?php

namespace App\Validation\Rules;

use App\Models\User;
use Respect\Validation\Rules\AbstractRule;

class EmailMatch extends AbstractRule
{

    public function validate($input)
    {
        return User::where('email', $input)->count() === 0; // Проверям имейл на одинаковость из списка в БД. считаем счисло совпадений, если >0 то false
    }

}

