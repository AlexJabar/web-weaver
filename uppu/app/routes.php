<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;

//для всех
$app->get('/', 'HomeController:index')->setName('home');



$app->get('/upload', 'UploadController:getUpload')->setName('upload');
$app->post('/upload', 'UploadController:postUpload');

$app->get('/files/{hash}', 'FileController:getFile')->setName('show'); // передаем в строке переменную хэш

//только для гостей
$app->group('', function () use ($app) {
    // можно use ($app) либо $this
    $app->get('/auth/signup', 'AuthController:getSignUp')->setName('auth.signup');
    $app->post('/auth/signup', 'AuthController:postSignUp');

    $app->get('/auth/signin', 'AuthController:getSignIn')->setName('auth.signin');
    $app->post('/auth/signin', 'AuthController:postSignIn');
})->add(new GuestMiddleware($container));

//только для залогининых юзеров
$app->group('', function () {
// создаем группу опред. URL доступных только по условию в AuthMiddleware
    $this->get('/auth/signout', 'AuthController:getSignOut')->setName('auth.signout');
    $this->get('/auth/password/change', 'PasswordController:getChangePassword')->setName('password.change');
    $this->post('/auth/password/change', 'PasswordController:postChangePassword');
})->add(new AuthMiddleware($container));



