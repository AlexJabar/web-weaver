<?php

namespace App\Controllers;

use \Slim\Views\Twig as View;
use App\Models\File;
use App\UploadHelper\FileException;
use App\UploadHelper\EmptyFileException;
//use Respect\Validation\Validator as v;

function formatBytes($bytes, $precision = 2) { 
    $units = array('Байт', 'Кб', 'Мб', 'Гб', 'Тб'); 
    $bytes = max($bytes, 0); 
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow = min($pow, count($units) - 1); 
    $bytes /= pow(1024, $pow);
    return round($bytes, $precision) . ' ' . $units[$pow]; 
} 






class UploadController extends Controller
{

    public function getUpload($request, $responce)
    {
        return $this->view->render($responce, 'uploading/upload.twig');//путь к шаблону задан в контейнере
    }

 //------------------------------------------------------------------------------------------------------------------   

    public function postUpload($request, $responce)
    {
        
        try {
            $upload = $this->upload_helper->loadFile($request->getUploadedFiles());

        
        } catch (FileException $e) {
            $this->flash->addMessage('error', 'Some error happend. Error code: '.$e->getMessage());
            return $responce->withRedirect($this->router->pathFor('home'));
        
        } catch (EmptyFileException $e) {
            $this->flash->addMessage('info', $e->getMessage());
            return $responce->withRedirect($this->router->pathFor('upload'));
        }

        if ($upload) {
            preg_match_all('/(?!\/)[0-9a-z]{20}(?=\.[a-z]*)/', $upload, $hash);
            
            $file = File::create([
                'url'      => str_replace('D:\Progs\openserver\OSPanel\domains\uppu\app\UploadHelper/../../public', 'http://uppu', $upload),
                'name'     => $request->getUploadedFiles()['file']->getClientFilename(),
                'type'     => $request->getUploadedFiles()['file']->getClientMediaType(),
                'size'     => formatBytes($request->getUploadedFiles()['file']->getSize()),
                'hash'     => $hash[0][0],
                'description' => $request->getParam('description')

                ]);
        }

        $this->flash->addMessage('info', 'File was successfully uploaded');
        return $responce->withRedirect($this->router->pathFor('home'));    
    }
}
