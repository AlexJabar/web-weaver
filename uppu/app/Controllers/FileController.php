<?php

namespace App\Controllers;

use \Slim\Views\Twig as View;
use App\Models\File;




class FileController extends Controller
{

    public function getFile($request, $responce, $args)
    {

        $hash = htmlspecialchars(strip_tags($args['hash']));
        
        $fileUrl = File::where('hash', '=', $hash)->value('url');
        
       

        return $this->view->render($responce, 'uploading/show.twig', [
                'url' => $fileUrl,

            ]);//путь к шаблону задан в контейнере
    }

}
