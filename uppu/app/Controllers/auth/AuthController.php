<?php

namespace App\Controllers\Auth;

use App\Models\User;//модель пользователя
use App\Controllers\Controller;//из-за того что лежит не в той же папке 
use Respect\Validation\Validator as v;//подключение класса валидатора

class AuthController extends Controller
{
    public function getSignIn($request, $responce)
    {
        return $this->view->render($responce, 'auth/signin.twig');
    }

    public function postSignIn($request, $responce)
    {
        $auth = $this->container->auth->attempt(
            $request->getParam('email'),
            $request->getParam('password')

        );

        if (!$auth) {

            $this->flash->addMessage('error', 'Enter valid info!');

            return $responce->withRedirect($this->router->pathFor('auth.signin'));
        }

        else return $responce->withRedirect($this->router->pathFor('home'));
    }
    
    public function getSignUp($request, $responce)
    {
        return $this->view->render($responce, 'auth/signup.twig');
    }

    public function postSignUp($request, $responce)
    {
        $validation = $this->validator->validate($request, [
            'email'    => v::noWhitespace()->notEmpty()->EmailMatch(),
            'name'     => v::noWhitespace()->notEmpty()->alpha(),
            'password' => v::noWhitespace()->notEmpty(),
            ]);

        
        
        if ($validation->failed()) {

            return $responce->withRedirect($this->router->pathFor('auth.signup')); // если валидация не прошла то откидываем назад
        }

        $user = User::create([
            'email' => $request->getParam('email'),
            'name' => $request->getParam('name'),
            'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
            ]);

        $this->flash->addMessage('info', 'You have been signed up');

        $this->auth->attempt($user->email, $request->getParam('password')); // сразу залогиниваемся

        return $responce->withRedirect($this->router->pathFor('home'));
    }

    public function getSignOut($request, $responce)
    {
        $this->container->auth->logout();

        return $responce->withRedirect($this->router->pathFor('home'));
    }          
}
