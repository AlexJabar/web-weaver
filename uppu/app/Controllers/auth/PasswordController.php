<?php

namespace App\Controllers\Auth;

use App\Models\User;//модель пользователя
use App\Controllers\Controller;//из-за того что лежит не в той же папке 
use Respect\Validation\Validator as v;//подключение класса валидатора

class PasswordController extends Controller
{
   public function getChangePassword($request, $responce)
   {
   		$this->view->render($responce,'auth/password.twig');
   }

   public function postChangePassword($request, $responce)
   {

   		
   		$validation = $this->validator->validate($request, [
   			'password_old' => v::noWhitespace()->notEmpty()->PasswordMatch($this->auth->user()->password), // название метода PasswordMatch есть названине класса
   			'password' => v::noWhitespace()->notEmpty(),
   		]);

   		if ($validation->failed()) {
   			return $responce->withRedirect($this->router->pathFor('password.change'));
   		}

   		$this->auth->user()->setPassword($request->getParam('password'));
   		$this->flash->addMessage('info', 'Your password was changed');
   		return $responce->withRedirect($this->router->pathFor('home'));
   } 

}
