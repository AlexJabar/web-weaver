<?php
namespace App\Controllers;

class Controller // Базовый контроллер от которого наследуются остальные. содержит контейнер.
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __get($prop) // Магический метод. если у контейнера есть $prop то его и вернет
    {
        if ($this->container->{$prop}) {
            return $this->container->{$prop};
        }
    }
}
