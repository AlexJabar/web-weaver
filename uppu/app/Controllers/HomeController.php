<?php

namespace App\Controllers;

use \Slim\Views\Twig as View;
use App\Models\File;

class HomeController extends Controller
{
    public function index($request, $responce)
    { 
    	$files = File::orderBy('created_at', 'desc')->take(50)->get();

        return $this->view->render($responce, 'home.twig', [
        	'files' => $files
        ]);
        //путь к шаблону задан в контейнере
    }
}
