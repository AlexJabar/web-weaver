<?php

namespace App\Auth;

use App\Models\User;

class Auth
{
    public function user()
    {
        return User::find($_SESSION['user']);
    }

    public function check()
    {
        return isset($_SESSION['user']);
    }

    public function attempt($email, $password)
    {
        //найти юзера по имейлу
        $user = User::where('email', $email)->first();

        if (!$user) {
            return false;
        }
        //проверка пароля на соответсвие
        if (password_verify($password, $user->password)) {
        //сохраняем юзера глобально
            $_SESSION['user'] = $user->id;
            return true;
        }

        else return false;    
    }


    public function logout()
    {
        unset($_SESSION['user']);
    }
}       