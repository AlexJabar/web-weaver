<?php

namespace App\UploadHelper;

//use Slim\Http\Request;
//use Slim\Http\Response;
use Slim\Http\UploadedFile;

class FileException extends \Exception { }
class EmptyFileException extends \Exception { }

function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(10)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . '/' . $filename);
    $url = $directory . '/' . $filename;

    return $url;
}





class UploadHelper 

{
    
    public function loadFile($uploadedFiles)
    { 
        
        $path = __DIR__ . '/../../public/files/'.date('o');
        if (!file_exists($path)) {
            mkdir($path);
        }

        $uploadedFile = $uploadedFiles['file'];

        if (empty($uploadedFile->file)) {
            throw new EmptyFileException('You did not choose file to upload!'); 
        }
        
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            return $filename = moveUploadedFile($path, $uploadedFile, $request);
        }

        throw new FileException($uploadedFile->getError());
    }
}
