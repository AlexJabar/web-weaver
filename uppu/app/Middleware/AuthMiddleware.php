<?php
//для ограничения доступа  к определенным URL
namespace App\Middleware;

class AuthMiddleware extends Middleware
{
    public function __invoke($request, $responce, $next)
    {
        // проверяем не залогинен ли юзер
        if (!$this->container->auth->check()) {
            $this->container->flash->addMessage('error', 'You need to be signed in');
            return $responce->withRedirect($this->container->router->pathFor('auth.signin'));
        }

        
        $responce = $next($request, $responce);
        return $responce;
    }

}
   
