@extends('templates.default')

@section('content')

    <div class="row justify-content-center">
        <div id="alef" class="col-8 test-process-question-container">


            @foreach($test->questions as $keyQ => $question)
                <div id="{{'q'.($keyQ+1)}}" class="questionDiv">
                    <h5 class="test-process-question-number">
                        Question #{{ $keyQ + 1 }}
                    </h5>
                    <p>{{ $question->body }}</p>

                    <form method="post" action="{{ route('tests.process', ['test' => $test]) }}" role="form">
                        @foreach($question->answers as $keyA => $answer)
                            <p class="test-process-answer">
                                {{ $answer->body}}
                                <input type="radio" class="test-process-radio" name="ans[{{ $keyQ }}]"
                                       value="{{ $answer->body }}">
                            </p>
                        @endforeach

                        <div class="test-process-control-buttons-container">
                            <span class="control-arrow back-arrow">
                                <i class="fas fa-angle-double-left"></i>
                            </span>
                            
                            <span class="control-arrow forward-arrow">
                                <i class="fas fa-angle-double-right"></i>
                            </span>
                        </div>


                        <input type="hidden" name="_token" value="{{ Session::token() }}">
                    </form>
                </div>
            @endforeach


        </div>
    </div>


@stop