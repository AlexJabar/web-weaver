@extends('templates.default')

@section('content')

<div id="video-bg">
        <video autoplay loop muted>
            <source src="/dbs.mp4" type="video/mp4">
        </video>
    </div>

<div class="row justify-content-start">
	<div class="col-7 test-creation-div">

<form method="post" action="{{ route('tests.create') }}" role="form" >
  
      <div class="form-group{{ $errors->has('testName') ? ' has-error' : ''}}" id="test_reg">
        <label for="testName">Test Name</label><br>
        <input type="text" name="testName" class="form-control" placeholder="You must name a new test">

        <label style="margin-top:15px" for="testDesc">Test Description</label><br>
        <textarea   name="testDesc" class="form-control"   cols="70" rows="3" maxlength="210" 
        placeholder="Here You can write a few words about test's subject or anything You want"></textarea> 
      </div>
  





  <div class="a1" id="a1">
  
    <label for="question[1]">Question #1</label><br>      
    <textarea name="question[1]" cols="70" rows="3" class="form-control" ></textarea>
    

      <div id="multi1" class="multi1">
      
        <label for="answer[1][]">a:</label>
          <input type="text" name="answer[1][]" class="answer">
          <input type="radio" name="correct[1][]" checked value="0">
         
          <br>
          
        <label for="answer[1][]">b:</label>
          <input type="text" name="answer[1][]" class="answer" >
          <input type="radio" name="correct[1][]" value="1" >
          <br>
          
        <label for="answer[1][]">c:</label>
          <input type="text" name="answer[1][]" class="answer" >
          <input type="radio" name="correct[1][]" value="2" >
          <br>
          
        <label for="answer[1][]">d:</label>
          <input type="text" name="answer[1][]" class="answer">
          <input type="radio" name="correct[1][]" value="3" >
          <br>
      </div>

      <div class="selectOnly" id="only1">
        <label for="answer[1][]">Answer</label>
          <input type="text" name="answer[1][]" class="answer">
      </div>
  
  </div>

  <fieldset class="test-creation-toggle">
    <div class="switch-toggle switch-candy">
      <input id="month" name="view" type="radio">
      <label for="month" onclick="displayQuestionType(0)">Multiple choice question</label>

      <input id="year" name="view" type="radio">
      <label for="year" onclick="displayQuestionType(1)">Only answer</label>

      <a></a>
    </div>
  </fieldset>


    <div id="controlButtons">
      <a href="#" id="add" class="btn btn-outline-primary">Add Question</a>
      <a href="#" id="remove" class="btn btn-outline-primary">Delete Question</a>
    </div>


<input type="submit" value="Create Test" class="btn btn-primary">
<input type="hidden" name="_token" value="{{ Session::token() }}">
</form>


</div>
</div>
@stop



