@extends('templates.default')

@section('content')


<div class="row">
	<div class="col-md" id="testHeader">
		<h5>Test name: {{ $test->test_name }}</h5>
		<p>Made by....</p>
		@if ($test->test_description)
			<p><strong>Test description:</strong><br> {{ $test->test_description }} </p>
		@endif
	</div>
	
</div>

<div class="row justify-content-center ">
	<div class="col" >
		<a class="btn btn-info" href="{{ route('tests.process', ['test' => $test])  }}" role="button" style="font: normal  20px Verdana, serif;">Start Test</a>
	</div>
</div>



@stop

