@extends('templates.default')

@section('content')
    <div id="video-bg">
        <video autoplay loop muted>
            <source src="/dbs.mp4" type="video/mp4">
        </video>
    </div>

<div class="row justify-content-center">
<div class="col-md-6 test-creation-menu">

<h2 class="main-header">Welcome to FasTests</h2>
    <div id="card">
        <div class="front">
            <div class="home-main-buttons-container" >
                <div class="home-main-btn"><a href="#" id="recent-tests-button">Show Recent</a></div>
                <div class="home-main-btn"><a href="{{ route('tests.create') }}">Create Online Test</a></div>
                <div class="home-main-btn"><a href="#" id="find-tests-button">Find Test</a></div>
            </div>
        </div>

        <div class="back">
            <div class="home-main-buttons-container back-home-main-buttons-container" >
                <div class="home-main-btn"><a href="#" id="back-to-menu">Back to Menu</a></div>
                <div class="home-main-btn home-main-search">
                    <form role="search" action="#" id="home-main-search-form">
                        <input type="text" name="query" class="home-main-search-input" placeholder="Search for test...">
                    </form>
                </div>
                <div class="home-main-btn"><a href="#" id="home-main-search-button">Search</a></div>
            </div>
        </div>
    </div>



</div>
</div>



    <div class="row justify-content-center">
    <div class="col-md-7 recent-tests-table-container">
        <h4 class="recent-tests-table-title">Recent posted tests:</h4>
        <table class="recent-tests-table">

            <tr>
                <th>Test Name</th>
                <th>Author</th>
                <th>Post Data</th>
            </tr>

            @foreach ($tests as $test)
                <tr>
                    <td class="recent-tests-table-name">
                        <a href="{{ route('tests.page', ['testId' => $test->id])  }}"> {{ $test->test_name}}</a>
                    </td>
                    <td>
                        @if (isset($test->user))
                            {{ $test->user }}
                        @else
                            Guest
                        @endif
                    </td>
                    <td>
                        {{ $test->created_at}}
                    </td>
                </tr>

            @endforeach

        </table>
    </div>
</div>



@stop