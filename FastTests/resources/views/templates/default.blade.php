<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>FastTests</title>
    <link rel="stylesheet"  href="/css/bootstrap.min.css">
    <link rel="stylesheet"  href="/css/toggle-switch.css?t=<?php echo(microtime(true)); ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet"  href="/css/app.css?t=<?php echo(microtime(true)); ?>">



    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>



</head>
<body>



@include('templates/navigation')



<div class="container">

        @yield('content')
</div>



<script src="/js/app.js?t=<?php echo(microtime(true)); ?>"></script>
<script src="/js/test.js?t=<?php echo(microtime(true)); ?>"></script>
<script src="/js/bootstrap.bundle.min.js?t=<?php echo(microtime(true)); ?>"></script>
<script src="/js/jquery.flip.min.js"></script>



</body>
</html>