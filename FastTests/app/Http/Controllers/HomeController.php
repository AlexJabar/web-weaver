<?php

namespace FastTests\Http\Controllers;

use Illuminate\Http\Request;
use FastTests\Test;

class HomeController extends Controller
{
     public function getHome()
    {
        $tests = Test::orderBy('created_at','desc')->take(10)->get();


         return view('home')->with('tests', $tests);
    }
}
