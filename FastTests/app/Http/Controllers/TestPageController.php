<?php

namespace FastTests\Http\Controllers;

use Illuminate\Http\Request;
use FastTests\Test;

class TestPageController extends Controller
{
    public function getTestPage($testId)
    {
        $test = Test::where('id', '=', $testId)->first();
        if (!$test) {
            abort(404);
        }


        

        return view('tests.page')->with('test', $test);
     
    }

}
