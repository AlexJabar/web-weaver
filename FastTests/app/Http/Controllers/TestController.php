<?php

namespace FastTests\Http\Controllers;

use Illuminate\Http\Request;
use FastTests\Test;
use Illuminate\Support\Facades\Hash;



class TestController extends Controller
{
    public function getNewTest()
    {
         return view('tests.create');
    }


    public function postNewTest(Request $request)
    {
        
        //Validation-------------------------------------------------------------

        /*$val = $this->validate($request, [
                'testName'   => 'required|max:50',
                'testDesc'   => 'max:500',
                'question.*' => 'required|max:500',
                'answer.*.*' => 'required|max:100',
            ]);*/


           
        //------------------------------------------------------------------------

        $test = Test::create([
            'test_name'          => $request->input('testName'),
            'test_description'   => $request->input('testDesc'),
            
             
        ]);
        
        foreach ($request->input('question') as $questionNum => $question)

        {   
            
            //создаем запись вопроса для теста через метод questions() модели Test
            $questionObj = $test->questions()->create(['body' => $question]);
            //массивы с ответами и инфа о правильном ответе
            $answers        = $request->input('answer')[$questionNum];
            $correctAnswer  = $request->input('correct')[$questionNum];

                foreach ($answers as $answerNum => $answer)

                {
                    //если блок ответова скрыт или не заполнен, то пропускаем данный ответ
                    if (!$answer) continue;

                    // критерий правильности для вопросов с вариантами ответов
                    $isCorrect = (current($correctAnswer) == $answerNum ) ? true : false;
                    // для вопросов без вариантов ответов (без радиопереключателя)
                    if (!current($correctAnswer)) {
                        $isCorrect = true;
                    }

                    //создаем запись ответа для вопроса через метод answers() модели Question
                    $questionObj->answers()->create([
                        'body'    => $answer,
                        'correct' => $isCorrect, 
                    ]);
                    
                }
        }



    }
}
