<?php

namespace FastTests;

use Illuminate\Database\Eloquent\Model;
use FastTests\Question;

class Test extends Model
{
   	protected $table = 'tests';

   	protected $fillable = [ 'test_name', 
   							'test_description', 
   							'test_time'];

   	public function questions()
    {
        return $this->hasMany('FastTests\Question', 'test_id');
    }
}
