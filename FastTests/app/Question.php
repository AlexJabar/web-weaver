<?php

namespace FastTests;

use Illuminate\Database\Eloquent\Model;
use FastTests\Test;
use FastTests\Answer;

class Question extends Model
{
    protected $table = 'questions';

   	protected $fillable = [ 'test_id', 
   							'body'];


   	public function test()
    {
        return $this->belongsTo('FastTests\Test', 'test_id');
    }						

    public function answers()
    {
        return $this->hasMany('FastTests\Answer', 'question_id');
    }	

}
