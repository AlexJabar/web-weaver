<?php

namespace FastTests;

use Illuminate\Database\Eloquent\Model;
use FastTests\Question;

class Answer extends Model
{
    protected $table = 'answers';

   	protected $fillable = [ 'question_id', 
   							'body',
   							'correct'];


    public function question()
    {
        return $this->belongsTo('FastTests\Question', 'question_id');
    }	



}
