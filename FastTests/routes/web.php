<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
    'uses' => '\FastTests\Http\Controllers\HomeController@getHome',
    'as' => 'home',
    'middleware' => ['guest'],
]);




Route::get('/test/new', [
    'uses' => '\FastTests\Http\Controllers\TestController@getNewTest',
    'as' => 'tests.create',
    'middleware' => ['guest'],
]);

Route::post('/test/new', [
    'uses' => '\FastTests\Http\Controllers\TestController@postNewTest',
    'as' => 'tests.create',
    'middleware' => ['guest'],

]);

Route::get('/test/{testId}', [
    'uses' => '\FastTests\Http\Controllers\TestPageController@getTestPage',
    'as' => 'tests.page',
    'middleware' => ['guest'],
]);

Route::get('/test/pass/{test}', [
    'uses' => '\FastTests\Http\Controllers\TestProcessController@getTestProcess',
    'as' => 'tests.process',
    'middleware' => ['guest'],
]);


Route::post('/test/pass/{test}', [
    'uses' => '\FastTests\Http\Controllers\TestProcessController@postTestProcess',
    'as' => 'tests.process',
    'middleware' => ['guest'],
]);
