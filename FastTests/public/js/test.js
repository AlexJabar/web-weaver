var k = 1;

$(document).ready(function(e) {
showQuestions();

$(".forward-arrow").click(function () {
	k++;
	showQuestions();
});

$(".back-arrow").click(function () {
	if (k > 1) {
		k--;
		showQuestions();
	}
});


$(".test-process-answer").click(function (e) {
    $target = $(e.target);

    $target.siblings().removeClass('chosen-answer');
    $target.siblings().each(function(){
		$(this).find('input').removeAttr('checked');

    });

    $target.addClass('chosen-answer');
    $target.find('input').attr('checked', 'checked');
});


});



showQuestions = function() {
	$('.questionDiv').each(function(){
    if ($(this).attr("id") !== `q${k}`) {
    	$(this).hide();
    } else $(this).show();
});
}

