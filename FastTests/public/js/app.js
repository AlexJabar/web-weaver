var i=1;

// тип вопроса
displayQuestionType = function(x)
{
if (x==0) {
document.getElementById(`multi${i}`).style.display='block';
document.getElementById(`only${i}`).style.display='none';
}
else {
document.getElementById(`multi${i}`).style.display='none';
document.getElementById(`only${i}`).style.display='block';
}
return;
}




$(document).ready(function(e) {

// главна страница, перехоод к таблице тестов

    $("#card").flip({trigger: 'manual'});

    $("#recent-tests-button").click(function (e) {
        $(".recent-tests-table-container").show();

        $('html, body').animate({ scrollTop: $(".recent-tests-table-container").offset().top }, 1500);

        $("html, body").promise().done(function(){
            $(".test-creation-menu").hide();
            $(".recent-tests-table-container").css("margin-top","0");
        });
    });


    $("#find-tests-button").click(function () {
        $('.back-home-main-buttons-container').show();
        $("#card").flip('toggle');

    });

    $('#back-to-menu').click(function () {

        $("#card").flip('toggle');

    });


    $('#home-main-search-button').on('click', function() {
      $('form#home-main-search-form').trigger('submit');
    });



//очистка при переключении
$('#onlySwitch' ).on('click', function(e){
   $(`#multi${i}`).wrap('<form>').closest('form').get(0).reset();
   $(`#multi${i}`).unwrap(); 
});

$('#multiSwitch' ).on('click', function(e){
   $(`#only${i}`).wrap('<form>').closest('form').get(0).reset();
   $(`#only${i}`).unwrap(); 
});


// Добавить форму вопроса
  $("#add").click(function (e) {
     i++;

    $("#a1").append(`<div><br><label for="question">Question #${i}</label><br>
    <textarea name="question[${i}]" cols="70" rows="5" class="form-control" ></textarea>
    
    
    
    <div id="multi${i}" class="multi1">

        <label for="answer[${i}][]">a:</label>
          <input type="text" name="answer[${i}][]" value="" id="answer_A_${i}">
          <input type="radio" name="correct[${i}][]" checked value="0">
          <br>
          
        <label for="answer[${i}][]">b:</label>
          <input type="text" name="answer[${i}][]" value="" id="answer_B_${i}">
          <input type="radio" name="correct[${i}][]" value="1">
          <br>
          
        <label for="answer[${i}][]">c:</label>
          <input type="text" name="answer[${i}][]" value="" id="answer_C_${i}">
          <input type="radio" name="correct[${i}][]" value="2"> 
          <br>
          
        <label for="answer[${i}][]">d:</label>
          <input type="text" name="answer[${i}][]" value="" id="answer_D_${i}">
          <input type="radio" name="correct[${i}][]" value="3">  
          <br>
    </div>

    <div id="only${i}" class="selectOnly">
      <label for="answer[${i}][]">Answer</label>
        <input type="text" name="answer[${i}][]" id="only_answer_${i}">
    </div></div>`);

   $("html, body").animate({ scrollTop: $(document).height() }, "slow");
  });
  
  // убрать форму воспроса
  $("#controlButtons").on('click','#remove', function(e) {
  if (i<2) 
    return
  else {
    $(`#multi${i}` || `#only${i}`).parent('div').remove();
    i--;
    }

    e.preventDefault();
  });
  

});

