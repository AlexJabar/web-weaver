<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;



class FriendController extends Controller
{
    public function getIndex()
    {

        $friends = Auth::user()->friends();
        $requests = Auth::user()->friendRequests();
       

        return view('friends.index')
        ->with('friends', $friends)
        ->with('requests', $requests);
    }

    public function getAdd($username)
    {
        $user = User::where('username', $username)->first(); //подбираем юзера которому отправили реквест

        if (!$user) {
            return redirect()->route('home')->with('info', 'No such user found');
        }

        if (Auth::User()->hasFriendRequestPending($user) || $user->hasFriendRequestPending(Auth::user())) {
            return redirect()->route('profile.index', ['username' => $user->username])
                             ->with('info', 'Friend request is already pending');
        }

        if (Auth::User()->isFriendWith($user)) {
            return redirect()->route('profile.index', ['username' => $user->username])
                             ->with('info', 'You are already friend with that user');
        }

        if (Auth::id() === $user->getId()) {
            return redirect()->route('profile.index', ['username' => $user->username]);                 
        }   

        Auth::user()->addFriend($user);

        return redirect()->route('profile.index', ['username' => $username])->with('info', 'Friend request send');
    }

    public function getAccept($username)
    {
        $user = User::where('username', $username)->first();

        if (!$user) {
            return redirect()->route('home')->with('info', 'No such user found');
        }

        if (!Auth::user()->hasFriendRequestReceived($user)) {
            return redirect()->route('home');
        }


        Auth::user()->acceptFriendRequest($user);

         return redirect()->route('profile.index', ['username' => $username])->with('info', 'You just added '.$username.' as your friend');

    }


    public function postDelete($username)
    {
        $user = User::where('username', $username)->first();

        if (!Auth::user()->isFriendWith($user)) {
            return redirect()->back();
        }

        Auth::user()->deleteFriend($user);
        return redirect()->back()->with('info', 'You are no longer fiends with '. $username);


    }

    


    
}
