<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Status;
use Auth;


class StatusController extends Controller
{
    public function postStatus(Request $request)
    {
        $this->validate($request, [
            'status' => 'required|max:1000',
        ]);

        Auth::user()->statuses()->create([ // создаем статус.метод statuses() автоматически заполняет user_id в таблице статусов
            'body' => $request->input('status'),
        ]);

        return redirect()->route('home')->with('info', 'Status posted');
    }
    

    public function postReply(Request $request, $statusId)
    {
        $this->validate($request, [
            "reply-{$statusId}" => 'required|max:1000'], [
                'required' => 'The reply body is required'
        ]);

        $status = Status::notReply()->find($statusId); // находит статус на который надо ответить

        if (!$status) {
            return redirect()->route('home');
        }

        if (!Auth::user()->isFriendWith($status->user) && Auth::id() !== $status->user->id) { // если это не друг и это не ты сам, то   ответить ты не сможешь
            return redirect()->route('home');
        }

        $reply = Status::create([ // ссоздает новый статус и ассоциирует его с запостившим его юзером

            'body' => $request->input("reply-{$statusId}")

        ])->user()->associate(Auth::user()); 

        $status->replies()->save($reply); // берет id $status и записывет его в parent_id нового статуса (ответа к статусу), потом записывает reply туда же

        return redirect()->back();
        
    } 



    public function getLike($statusId)
    {
         $status = Status::find($statusId);

          if (!$status) {
            return redirect()->route('home');
        }

        if (!Auth::user()->isFriendWith($status->user)) { // если залогининый юзер птается лайкнуть не друга
            return redirect()->route('home')->with('info', 'Can\'t like that user');
        }

        if (Auth::user()->hasLikesStatus($status)) {
            return redirect()->back();
        }

        $like = $status->likes()->create([]);// создаем лайк.метод likes() автоматически заполняет likeable_id(в наш случае айди статуса) в таблице лайков
        Auth::user()->likes()->save($like); // добавляем айди юзера, который запостил лайк в таблицу лайков
        return redirect()->back();
    } 
}

 