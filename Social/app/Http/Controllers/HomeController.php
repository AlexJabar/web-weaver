<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Status;

class HomeController extends Controller
{
    public function index()
    {

        if (Auth::check()) { // находит твои статусы или статусы твоих друзей

            $statuses = Status::notReply()->where(function ($query) { 
                return $query->where('user_id', Auth::id())->orWhereIn('user_id', Auth::user()->friends()->lists('id'));
            })
            ->orderBy('created_at', 'desc')
            ->paginate(5);


            return view('timeline.index')
                 ->with('statuses', $statuses);
        }
        
        return view('home');
    }
}
