<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    public function getSignup()
    {
        return view('signup');
    }

    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email|max:255',
            'username' => 'required|unique:users|alpha_dash|max:20',
            'password' => 'required|min:6'
        ]);

        User::create([
            'username' => $request->input('username'),
            'email'    => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        return redirect()->route('home')->with('info','Your account has been created');

    }

    public function getSignin()
    {
        return view('signin');
    }

    public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);


        if (!Auth::attempt($request->only(['email', 'password']), $request->has('remember'))) {
            return redirect()->back()->with('info', 'Email or password are incorrect');
        }

        return redirect()->route('home')->with('info', 'You have been signed in');
    }

    public function getSignout()
    {
        Auth::logout();
        
        return redirect()->route('home');
    }




}
