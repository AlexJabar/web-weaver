<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Status extends Model
                                    
{
    protected $table = 'statuses'; 

    protected $fillable = ['body'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function scopeNotReply($query) // используется для фильтрации запроса

    {
        return $query->whereNull('parent_id'); // ищем только записи где parent_id пуст, то есть ищем только статусы, а не ответы к ним
    }

    public function replies()
    {
        return $this->hasMany('App\Models\Status', 'parent_id');
    }

    public function likes()
    {
        return $this->morphMany('App\Models\Like', 'likeable'); //Связь лайков со статусами, второй аргумент - имя метода в модели лайков
    }
  

}