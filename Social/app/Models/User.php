<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Models\Status;



class User extends Model implements AuthenticatableContract
                                    
{
    use Authenticatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'first_name', 'second_name', 'location'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function getName()
    {
        if ($this->first_name && $this->second_name) {
            return "{$this->first_name} {$this->second_name}";
        }

        if ($this->first_name) {
            return $this->first_name;
        }

        return null;
    }

    public function getNameOrUsername()
    {
        return $this->getName() ? : $this->username;
    }

    public function getFirstNameOrUsername()
    {
        return $this->first_name ? : $this->username;
    }


    public function getAvatarUrl()
    {
        return "https://www.gravatar.com/avatar/{{ md5($this->email) }}?d=mm";
    }

//--------------------------------------------------------------------------
    public function statuses()
    {
        return $this->hasMany('App\Models\Status', 'user_id');
    }
//--------------------------------------------------------------------------
    public function likes()
    {
        return $this->hasMany('App\Models\Like', 'user_id');
    }

//----------------------------------------------------------------------------
    public function friendsOfMine() // друзья этого юзера
    {
        
        
        return $this->belongsToMany('App\Models\User', 'friends', 'user_id', 'friend_id'); 
    }

    public function friendOf()// находит тех, у кого юзер в друзьях
    {
        return $this->belongsToMany('App\Models\User', 'friends', 'friend_id', 'user_id'); 
    }
    

    public function friends()// находит "добавивших этого юзера" друзей
    {
        return $this->friendsOfMine()->wherePivot('accepted', true)->get()
                    ->merge($this->friendOf()->wherePivot('accepted', true)->get());  // чтобы не создавать две зеркальные записи в таблице используется ->merge....
    }

    public function friendRequests()
    { //находит юзеров которые кинули реквест залогиненому юзеру

        return $this->friendsOfMine()->wherePivot('accepted', false)->get(); 
     }

    public function friendRequestPending()
     {
        // находит юзеров, которым кинул реквест залогиненый юзер
        return $this->friendOf()->wherePivot('accepted', false)->get(); 
     }

    public function hasFriendRequestPending(User $user)// Кидал ли залогиненый юзер реквес ОПРЕДЕЛЕННОМУ юзеру 
     {
        return (bool) $this->friendRequestPending()->where('id', $user->id)->count(); 
     }

    public function hasFriendRequestReceived(User $user)// Кидал ли ОПРЕДЕДЕННЫЙ юзер реквест залогиненому юзеру
     {
        return (bool) $this->friendRequests()->where('id', $user->id)->count(); 
     }

//---------------------------------------------------------------------------------------------

    public function addFriend(User $user) // нажавший добавать пользователя попадает в поле freind_id таблицы friends,
    // в user_id попадает $user->id
     {
        $this->friendOf()->attach($user->id); 
     }

    public function deleteFriend(User $user) 
     {
        $this->friendOf()->detach($user->id);
        $this->friendsOfMine()->detach($user->id); 
     }


    public function acceptFriendRequest(User $user)
     {
        $this->friendRequests()->where('id', $user->id)->first()->pivot->update([
                'accepted' => true,
            ]);
     }

    public function isFriendWith(User $user)
     {
        return (bool) $this->friends()->where('id', $user->id)->count();
     }

     public function getId()
     {
        return $this->id;
     }

//-----------------------------------LIKES------------------
    public function hasLikesStatus(Status $status) // лайкнул ли пользователь статус
    {
        return (bool) $status->likes->where('user_id', $this->id)->count();
    }


}



