<div class="media">
	<a href="{{ route('profile.index', ['username' => $status->user->username]) }}" class="pull-left">
		<img src="{{ $status->user->getAvatarUrl() }}" 
		     alt="{{ $status->user->getNameOrUsername() }}" class="media-object">
	</a>
	<div class="media-body">
		<h4 class="media-heading"><a href="{{ route('profile.index', ['username' => $status->user->username]) }}">
										   {{ $status->user->getNameOrUsername() }}</a></h4>
		<p>{{$status->body}}</p>
		<ul class="list-inline">
			<li>{{ $status->created_at->diffForHumans()}}</li> 

			@if ($status->user->id !== Auth::id())
				<li><a class="fa fa-thumbs-up"  href="{{ route('status.like', ['statusId' => $status->id]) }}"></a></li>
				<li> 
					@if ($status->likes->count())
						{{ $status->likes->count() }}
					@endif
				</li>
			@endif

		</ul>




	@foreach ($status->replies as $reply)

		<div class="media" >
			<a href="{{ route('profile.index', ['username' => $reply->user->username]) }}" class="pull-left">
				<img src="{{ $reply->user->getAvatarUrl() }}" 
				     alt="{{ $reply->user->getNameOrUsername() }}" class="media-object">
			</a>
			<div class="media-body">
				<h5 class="media-heading"><a href="{{ route('profile.index', ['username' => $reply->user->username]) }}">
					                               {{ $reply->user->getNameOrUsername() }}</a></h5>
				<p>{{$reply->body}}</p>
				<ul class="list-inline">
					<li>{{ $reply->created_at->diffForHumans()}}</li>

					@if ($reply->user->id !== Auth::id())
						<li><a class="fa fa-thumbs-up" href="{{ route('status.like', ['statusId' => $reply->id]) }}"></a></li>
						<li>
							@if ($reply->likes->count())
								{{ $reply->likes->count() }}
							@endif
						</li>
					@endif

				</ul>
			</div>
		</div>

	@endforeach






		<form role="form" action="{{ route('status.reply', ['statusId' => $status->id]) }}" method="post" style="padding-top: 10px">
			<div class="form-group {{ $errors->has("reply-{$status->id}") ? ' has-error' : ''}}"> <!--reply-{$status->id} выбираем поле 																						с ошибкой -->
				<textarea name="reply-{{ $status->id }}" class="form-control" rows="2" 
				          placeholder="Reply to this status"></textarea>

				@if ($errors->has("reply-{$status->id}"))
					<span class="help-block">{{$errors->first("reply-{$status->id}")}}</span>
				@endif

			</div>
			<input type="submit" value="Reply" class="btn btn-default btn-sm">
			<input type="hidden" name="_token" value="{{ Session::token() }}">
		</form>
	</div>
</div>