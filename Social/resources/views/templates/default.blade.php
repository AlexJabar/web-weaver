<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Social</title>
	<link rel="stylesheet"  href="/vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
	<link href='/css/style1.css'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	@include('templates.navigation') 

	<div class="container">
		@include('templates.alerts')
		@yield('content')

	</div>
	
</body>
</html>