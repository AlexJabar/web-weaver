@extends('templates.default')

@section('content')

<div class="row" >
	<div class="col-lg-6" >
		
				<form action="{{ route('profile.edit') }}" method='post' class="form-vertical" role="form">
					<div class="row">
						<div class="col-lg-6" >
							<div class="form-group{{ $errors->has('first_name') ?' has-error' : ''}}">
								<label for="first_name" class="control-label">First Name</label>
								<input type="text" name="first_name" id="first_name"  class="form-control" 
								value="{{ old('first_name') ? : Auth::user()->first_name }}">

									@if ($errors->has('first_name'))
										<span class="help-block">{{$errors->first('first_name')}}</span>
									@endif
							</div>	
				    	</div>


						<div class="col-lg-6" >
							<div class="form-group{{ $errors->has('second_name') ?' has-error' : ''}}">
								<label for="second_name" class="control-label">Second Name</label>
								<input type="text" name="second_name" id="second_name"  class="form-control"
								value="{{ old('second_name') ? : Auth::user()->second_name }}">
									@if ($errors->has('second_name'))
										<span class="help-block">{{$errors->first('second_name')}}</span>
									@endif
							</div>	
				    	</div>
					</div>

				<div class="form-group{{ $errors->has('location') ?' has-error' : ''}}">
						<label for="location" class="control-label">Location</label>

						<input type="text" name="location" id="location"  class="form-control"
						value="{{ old('location') ? : Auth::user()->location}}">
							@if ($errors->has('location'))
								<span class="help-block">{{$errors->first('location')}}</span>
							@endif
					</div>

					<div class="form-group">
					

						<button type="submit" class="btn btn-default">Update</button>
					</div>		

					<input type="hidden" name="_token" value="{{ Session::token() }}">
				</form>
		

	</div>
</div>




@stop
