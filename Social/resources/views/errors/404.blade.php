@extends('templates.default')

@section('content')
<h3>Ops, can't find that page</h3>
<a href="{{ route('home')}}">Go Home</a>
@stop
