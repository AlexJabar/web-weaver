
@extends('templates.default')

@section('content')

<div class="row" >
	<div class="col-md-5 col-md-offset-3" >
		<div class="panel panel-default">
			
			<div class="panel-body" >
				<form action="{{ route('signup') }}" method='post' autocomplete="off">
					
					<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" value="{{old('email') ? :''}}">

							@if ($errors->has('email'))
								<span class="help-block">{{$errors->first('email')}}</span>
							@endif
					</div>
					


					<div class="form-group{{ $errors->has('username') ? ' has-error' : ''}}">
						<label for="username">Choose Username</label>
						<input type="text" name="username" id="username"  class="form-control" value="{{old('username') ? :''}}">

							@if ($errors->has('username'))
								<span class="help-block">{{$errors->first('username')}}</span>
							@endif
					</div>
							



					<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
						<label for="password">Choose Password</label>
						<input type="password" name="password" id="password"  class="form-control">

							@if ($errors->has('password'))
								<span class="help-block">{{$errors->first('password')}}</span>
							@endif
					</div>	



					<div class="form-group">
						<button type="submit" class="btn btn-outline-dark" data-toggle="button" aria-pressed="false" autocomplete="off" >Sign Up
						</button>
					</div>	

					<input type="hidden" name="_token" value="{{ Session::token() }}">
				</form>
			</div>			
		</div>
	</div>
</div>

@stop