<div class="media">
	<a class="pull-left" href="{{ route('profile.index', ['username' => $user->username]) }}" scr=""> 
		<img class="media-object" src="{{ $user->getAvatarUrl() }}" alt="{{ $user->getNameOrUsername() }}">
	</a>
	<div class="media-body">
		<h4 class="media-heading"><a href="{{ route('profile.index', ['username' => $user->username]) }}">{{ $user->getNameOrUsername() }}</a></h4>
		@if ($user->loaction)
			<p>{{ $user->location }}</p>
		@endif
	</div>
</div>