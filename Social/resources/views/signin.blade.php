
@extends('templates.default')

@section('content')

<div class="row" >
	<div class="col-lg-6" >
		
				<form action="{{route('signin')}}" method='post' class="form-vertical" role="form">
					
					<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control">

						@if ($errors->has('email'))
							<span class="help-block">{{$errors->first('email')}}</span>
						@endif
					</div>



					<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
						<label for="password">Choose Password</label>
						<input type="password" name="password" id="password"  class="form-control">

						@if ($errors->has('password'))
							<span class="help-block">{{$errors->first('password')}}</span>
						@endif
					</div>	


					<div class="checkbox">
						<label>
							<input type="checkbox" name="remember"> Remember me
						</label>
					</div>


					<div class="form-group">
						<button type="submit" class="btn btn-outline-dark">Sign In</button>
					</div>	

					<input type="hidden" name="_token" value="{{ Session::token() }}">
				</form>
		
	</div>
</div>

@stop